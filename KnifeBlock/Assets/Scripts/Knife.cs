using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Knife : MonoBehaviour
{
    public float speed = 30f;
    Vector2 velocity = Vector2.right * Vector2.up;
    public GameObject child;
    public Transform parent;
    public GameObject knife2;
    public GameObject knife3;
    public GameObject knife4;
    public GameObject knife5;
    static int knifeCount = 1;


    public void OnTriggerEnter2D(Collider2D collider)
    {

        if (collider.tag == "Block")
        {
            
            speed = 0;
            child.transform.SetParent(parent);

            knifeCount += knifeCount;
            Debug.Log(knifeCount);
            

            if (knifeCount == 2)
            {
                knife2.SetActive(true);
            }
            else if (knifeCount == 4)
            {
                knife3.SetActive(true);
            }
            else if (knifeCount == 8)
            {
                knife4.SetActive(true);
            }
            else if (knifeCount == 16)
            {
                knife5.SetActive(true);
            }
            else if (knifeCount == 32)
            {
                SceneManager.LoadScene(2);
            }
        }
        else if(collider.tag == "Knife")
        {
            SceneManager.LoadScene(3);
        }
    }
    

    private void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            transform.position += transform.right * speed * Time.deltaTime;
        }
    }
}
